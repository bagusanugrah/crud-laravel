<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pages.create');
    }

    public function store(Request $request){// CREATE/INPUT DATA #Crud (create)
        $request->validate([
            'judul' => 'required|max:255',
            'isi' => 'required|max:255'
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul"=>$request["judul"],
            "isi"=>$request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan!');
    }

    public function index(){//READ ALL DATA FROM DATABASE
        $pertanyaan = DB::table('pertanyaan')->get(); //SELECT * FROM pertanyaan
        return view('pages.index', compact('pertanyaan')); //compact untuk mengirim data pertanyaan ke index
    }

    public function show($pertanyaan_id){//READ SPECIFIC DATA #cRud (read)
        $pertanyaan = DB::table('pertanyaan')->where('pertanyaan_id', $pertanyaan_id)->first();
        return view('pages.show', compact('pertanyaan'));
    }

    public function edit($pertanyaan_id){
        $pertanyaan = DB::table('pertanyaan')->where('pertanyaan_id', $pertanyaan_id)->first();
        return view('pages.edit', compact('pertanyaan'));
    }

    public function update($pertanyaan_id, Request $request){//#crUd (update)
        $request->validate([
            'judul' => 'required|max:255',
            'isi' => 'required|max:255'
        ]);

        $query = DB::table('pertanyaan')
                    ->where('pertanyaan_id',$pertanyaan_id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);
        
        return redirect('/pertanyaan')->with('success','Berhasil Update pertanyaan!');
    }

    public function destroy($pertanyaan_id){//#cruD (delete)
        $query = DB::table('pertanyaan')->where('pertanyaan_id',$pertanyaan_id)->delete();
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Dihapus!');
    }
}
