<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProsesController extends Controller
{
    public function index(){
        return view('pages.index');
    }

    public function data(){
        return view('pages.data');
    }
}
