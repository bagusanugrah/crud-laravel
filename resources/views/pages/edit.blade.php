@extends('template.master')

@section('title')
<title>Edit</title>
@endsection

@section('content')
<div class="mt-3 ml-3 mr-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Post {{$pertanyaan->pertanyaan_id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$pertanyaan->pertanyaan_id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" value="{{ old('judul',$pertanyaan->judul) }}" name="judul" placeholder="Masukkan Judul">
                    @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                  <div class="form-group">
                    <label for="isi">Isi</label>
                    <input type="text" class="form-control" id="isi" value="{{ old('isi',$pertanyaan->isi) }}" name="isi" placeholder="Isi">
                    @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
</div>
@endsection