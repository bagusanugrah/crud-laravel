@extends('template.master')

@section('title')
<title>Index</title>
@endsection

@section('content')
<div class="mt-3 ml-3 mr-3">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                  <div class="alert alert-success">
                    {{ session('success') }}
                  </div>
                @endif
                <a class="btn btn-primary" href="/pertanyaan/create">Buat Pertanyaan Baru</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pertanyaan as $key => $tanya)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td>{{ $tanya->judul }}</td>
                      <td>{{ $tanya->isi }}</td>
                      <td style="display: flex;">
                        <a href="/pertanyaan/{{$tanya->pertanyaan_id}}" class="btn btn-info btn-sm ml-1 mr-1">show</a>
                        <a href="/pertanyaan/{{$tanya->pertanyaan_id}}/edit" class="btn btn-default btn-sm ml-1 mr-1">edit</a>
                        <form action="/pertanyaan/{{$tanya->pertanyaan_id}}" method="POST">
                          @csrf
                          @method('DELETE')
                          <input type="submit" value="delete" class="btn btn-danger btn-sm ml-1 mr-1">
                        </form>
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="4" align="center">Tidak ada pertanyaan</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
</div>  
</div>
@endsection