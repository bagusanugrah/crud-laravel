@extends('template.master')

@section('title')
<title>Buat Pertanyaan</title>
@endsection

@section('content')
<div class="mt-3 ml-3 mr-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Buat Pertanyaan Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" value="{{ old('judul','') }}" name="judul" placeholder="Masukkan Judul">
                    @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                  <div class="form-group">
                    <label for="isi">Isi</label>
                    <input type="text" class="form-control" id="isi" value="{{ old('isi','') }}" name="isi" placeholder="Isi">
                    @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Buat</button>
                </div>
              </form>
            </div>
</div>
@endsection